/**
 * Created by mgr on 1/2/14.
 */
var docServices = angular.module('contactResource', ['ngResource']);

docServices.factory('Contact', ['$resource',
    function ($resource) { 
        var contact = $resource('https://api.mongolab.com/api/1/databases/:db/collections/:collection/:id',
        {apiKey:'MNY3w9zXYlR3nMEKFUjkzTPkSWV1AMpr', db:'heccajob', collection:'dedvecja', id:'@_id.$oid'}, 
        {
            update: {method : "PUT"},            
            query: {method: 'GET', params: {}, isArray: true},
            count: {method: 'GET', params:{c:true}, isArray : false}
        });

        contact.prototype.update = function(cb, errorcb) {
            return contact.update({id: this._id.$oid}, angular.extend({}, this, {_id:undefined}), cb, errorcb);
        };

        contact.prototype.upsert = function(cb, errorcb){
            if(this._id){
                console.log('Updating' + this);  
                this.update(cb, errorcb);
            }else{        
                console.log('Saving');
                console.log(this._id);        
                this.$save(cb, errorcb);
            }
        };

        return contact;
    }]);