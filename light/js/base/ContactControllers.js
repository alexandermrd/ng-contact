var ContactControllers = angular.module('ContactControllers', ['contactResource']);

ContactControllers.controller('ListController', ['$scope', 'Contact', '$location', '$routeParams',
    function ($scope, Contact, $location, $routeParams) {
        $scope.page = ($routeParams.page || 0)*1;
        $scope.fw_paging_enabled = $scope.bw_paging_enabled = false;
        var contacts_per_page = 5;   

        $scope.contactList = Contact.query({s:{'defaultPhone':1}, l:contacts_per_page, sk: contacts_per_page*$scope.page});
        Contact.count({},
            function(result) {
                //TODO: fix raw string response handing
                var response = "";
                for (var c = 0, reached = false; !reached; c++){
                    if (result[c] !== undefined){
                        response += result[c];
                    }
                    else{
                        reached = true;
                    }
                }
                $scope.total_count = response * 1;
                if ($scope.page > 0){
                  $scope.bw_paging_enabled = true;  
                }
                if (($scope.page + 1) * contacts_per_page < $scope.total_count){
                   $scope.fw_paging_enabled = true; 
                }

                $scope.listFrom = contacts_per_page * $scope.page;
                $scope.listTo = $scope.listFrom + contacts_per_page;//FIXME: not true at the end of list
            });

        $scope.createContact = function(){
            $location.path("/contact/new");
        };

        $scope.editContact = function(index){
             $location.path("/contact/" + $scope.contactList[index]._id.$oid);
        };

        $scope.deleteContact = function(index){
           $scope.contactList[index].$remove(function(){
                $scope.contactList.splice(index,1);
            }); 
        };

        $scope.switchPage = function(forward){
            var toPage = $scope.page*1 + (forward ? 1 : -1);
            if (toPage >= 0 && 
                (($scope.bw_paging_enabled && (!forward)) ||
                    ($scope.fw_paging_enabled && forward))){
                $location.path("/list/"+toPage);
            }
        };

        $scope.addRandomContacts = function(){
            for (var k = 0; k < 200; k++){
                var x = ((new Date()).getTime())+"";
                var nameIndex = x[x.length-1]*1;
                var names = ["Uncle Tom Cobley", "Joe Bloggs", "Fred Nerks", "Waikikamukau",
                "Juan Perez", "Schmilblick", "La mama dracului"];
                var phones = [];
                for (var i = 0; i < 3; i++){
                    phones.push({number:x.substr(4-i, x.length), isDefault: (i === 0)});
                }
                var c = new Contact(
                    {name:names[nameIndex%names.length],
                     phones:phones, defaultPhone: phones[0].number});
                c.$save();
                $scope.contactList.push(c);
            }
        };
    }
]);

ContactControllers.controller('DetailsController',
    ['$scope', '$http', '$routeParams', '$location', 'Contact',
        function ($scope, $http, $routeParams, $location, Contact) {
            var generateUrl = function (docId) {
                return "http://lorempixel.com/200/200/people/" + docId + "?anticache=" + Math.random();
            };

            var contactId = $routeParams.contactId;
            if (contactId == 'new'){
                $scope.contact = new Contact({});
                $scope.contact.phones = [{number:"", isDefault:true}];
            }
            else{
               $scope.contact = Contact.get({id:contactId});
            }
            $scope.cPhoto = generateUrl(contactId);

            $scope.changeDefaultPhone = function(phoneIndex){
                console.log(phoneIndex + ", " + $scope.phones);
                for (var i = 0; i < $scope.contact.phones.length; i++){
                    $scope.contact.phones[i].isDefault = (i == phoneIndex);
                }
            };

            $scope.addPhone = function() {
              $scope.contact.phones.push({number:"", isDefault:false});
            };

            $scope.save = function() {
                for (var i = 0; i < $scope.contact.phones.length; i++){
                    if ($scope.contact.phones[i].isDefault){
                       $scope.contact.defaultPhone =  $scope.contact.phones[i].number;
                       break;
                    }
                }
                $scope.contact.upsert(function(){
                    $location.path('/list');                    
                }, function(e){
                    alert("Failed to update");
                });
            };

            $scope.cancelSaving = function(){
                $location.path('/list');
            };
        }]);
