var psiApp = angular.module('contactapp', ['ngRoute', 'ContactControllers']);

psiApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/list/:page?', {
                templateUrl: 'partials/outgoing/List.html',
                controller: 'ListController'
            }).
            when('/contact/:contactId', {
                templateUrl: 'partials/outgoing/Edit.html',
                controller: 'DetailsController'
            }).
            otherwise({
                redirectTo: '/list'
            });
}]);